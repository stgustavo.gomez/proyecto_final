import React, {useEffect, useRef} from 'react';
import { StyleSheet, Text, View, Animated, TextInput, Pressable, Keyboard, Image } from 'react-native';

import { useNavigation, useNavigationState } from '@react-navigation/native';


const HEADER_MAX_HEIGHT = 300;
const HEADER_MIN_HEIGHT = 80;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;



const MovieHeader = ({title = 'Titulo'} : props) => {


    const navigationState = useNavigationState(state => state);
    const navigation = useNavigation();

    //CONSTANTES DE ANIMACIONES
    /* const headerHeight = scrollY.interpolate({
        inputRange: [0, HEADER_SCROLL_DISTANCE],
        outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
        extrapolate: 'clamp',
      }); */


    const bottomSearchAnim = useRef(new Animated.Value(0)).current;

    

     //REFERENCIAS INPUT
     const inputEl = useRef(null);


    //FUNCIONES ANIMACIONES
    const onScreenTouch = () => {
        Animated.timing(
            bottomSearchAnim,
          {
            toValue: 0,
            duration: 50,
            useNativeDriver: false
          }
        ).start();

        Keyboard.dismiss();
    }

    const onSearchTouch = () => {
        Animated.timing(
            bottomSearchAnim,
          {
            toValue: -35,
            duration: 50,
            useNativeDriver: false
          }
        ).start();

        inputEl.current.focus();

    }


    const onBackTouch = () => {

        navigationState.index > 0 ? navigation.goBack() : null;
        
    }
    
    
   


    return (
        
            <Animated.View style={[styles.header]}>

                <Animated.Image
                    style={[
                    styles.backgroundImage,
                    ]}
                    source={require('../../assets/movies/headerImg/Moonlight.jpg')}
                />

                <View style={styles.headerCont}>
                    <Pressable style={styles.imageSearchCont} onPress={onBackTouch}>
                        <Image
                        style={{width: 20, height: 20}}
                        source={require('../../assets/TabIcons/backButton.png')}/>
                    </Pressable>

                    <Animated.Text style={styles.headerTitle}>
                        {title}
                    </Animated.Text>
                </View>

            </Animated.View>
    )
}

export default MovieHeader;

export interface props {
    title: string,
    scrollY?: Animated.Value,
}

const styles = StyleSheet.create({
    header: {
       position: 'relative',
        top: 0,
        height: HEADER_MAX_HEIGHT,
        zIndex: 0,
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: '100%',
        height: HEADER_MAX_HEIGHT,
        resizeMode: 'cover',
      },
      headerCont:{
          marginTop: 50,
          marginLeft: 20,
      },
      headerTitle: {
          fontSize: 30,
          marginTop: 10,
          color: '#FFFFFF'
      }
   
  });