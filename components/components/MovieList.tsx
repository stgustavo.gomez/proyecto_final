import React from 'react';
import { PointPropType, StyleSheet, Text, View, FlatList, Image, Pressable } from 'react-native';
import { useNavigation} from '@react-navigation/native';

const DATA = [
    {
      id: '1',
      src: require('../../assets/movies/premiadas/premiada1.jpg'),
    },
    {
      id: '2',
      src: require('../..//assets/movies/premiadas/premiada2.jpg'),
    },
    {
      id: '3',
      src: require('../../assets/movies/premiadas/premiada3.jpg'),
    },
    {
        id: '4',
        src: require('../../assets/movies/premiadas/premiada4.jpg'),
    },
  ];

 
const MovieList = ({titleList = 'Películas'}: MovieListProp) => {

    const navigation = useNavigation();

    

    const renderItem = ({ item, index}: interProp) => {
        
       return (
            <Pressable  onPress={() => navigation.navigate('Pelicula')} style={{width: 130, height: 130, marginLeft: 15}}>
                <Image style={{width: 130, height: 130}} source={item.src}/>
            </Pressable>
        )
        
    };

    return (
        <View style={styles.Cont}>
            <Text style={styles.titleTxt}>
                {titleList}
            </Text>
            <View style={{paddingTop: 15}}>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    horizontal = {true}
                    data={DATA}
                    renderItem={renderItem}
                    keyExtractor={item => item.id}
                />
            </View>

        </View>
    )
}

export default MovieList;

export interface interProp {
    item?: [],
    index?: number,
}

export interface MovieListProp {
    titleList?: string,
    navigation?: Navigator,
}

const styles = StyleSheet.create({
    Cont: {
       
    },
    titleTxt:{
        position: 'relative',
        left: 15,
        paddingTop: 20,
    }
})