import React, {useEffect, useRef} from 'react';
import { StyleSheet, Text, View, Animated, TextInput, Pressable, Keyboard } from 'react-native';


const HEADER_MAX_HEIGHT = 150;
const HEADER_MIN_HEIGHT = 80;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

const HEADER_MAX_FONT = 30;
const HEADER_MIN_FONT = 15;
const HEADER_SCROLL_FONT = HEADER_MAX_FONT - HEADER_MIN_FONT;

const TITLE_MAX_MARGIN = 100;
const TITLE_MIN_MARGIN = 0;
const TITLE_SCROLL_MARGIN = HEADER_MAX_FONT - HEADER_MIN_FONT;

const MAX_SEARCH_SZ = 30;
const MIN_SEARCH_SZ = 15;
const SEARCH_SCROLL = MAX_SEARCH_SZ - MAX_SEARCH_SZ;


const SectionHeader = ({title = 'Titulo', scrollY} : props) => {

    //CONSTANTES DE ANIMACIONES
    const headerHeight = scrollY.interpolate({
        inputRange: [0, HEADER_SCROLL_DISTANCE],
        outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
        extrapolate: 'clamp',
      });

    const headerFont = scrollY.interpolate({
        inputRange: [0, HEADER_SCROLL_FONT+50],
        outputRange: [HEADER_MAX_FONT, HEADER_MIN_FONT],
        extrapolate: 'clamp',
    });

    const marginTitle = scrollY.interpolate({
        inputRange: [0, TITLE_SCROLL_MARGIN+50],
        outputRange: [TITLE_MAX_MARGIN, TITLE_MIN_MARGIN],
        extrapolate: 'clamp',
    });

    const search_sz = scrollY.interpolate({
        inputRange: [0, SEARCH_SCROLL+70],
        outputRange: [ MAX_SEARCH_SZ, MIN_SEARCH_SZ],
        extrapolate: 'clamp',
    });

    const bottomSearchAnim = useRef(new Animated.Value(0)).current;

    

     //REFERENCIAS INPUT
     const inputEl = useRef(null);


    //FUNCIONES ANIMACIONES
    const onScreenTouch = () => {
        Animated.timing(
            bottomSearchAnim,
          {
            toValue: 0,
            duration: 50,
            useNativeDriver: false
          }
        ).start();

        Keyboard.dismiss();
    }

    const onSearchTouch = () => {
        Animated.timing(
            bottomSearchAnim,
          {
            toValue: -35,
            duration: 50,
            useNativeDriver: false
          }
        ).start();

        inputEl.current.focus();

    }

    
    
   


    return (
        
            <Animated.View style={[styles.header, {height: headerHeight}]}>
                   <Pressable style={{width: '100%', height: '100%'}} onPress={onScreenTouch}>   
                        <View style={styles.headerCont}>
                            <View style={styles.headerElCont}>
                                <Animated.Text style={[styles.headerTitle, {fontSize: headerFont, right: marginTitle}]}>
                                    PELÍCULAS
                                </Animated.Text>
                                <Pressable style={styles.imageSearchCont} onPress={onSearchTouch}>
                                    <Animated.Image style={[styles.imageSearch, {width: search_sz, height: search_sz}]}
                                    source={require('../../assets/TabIcons/search.png')}/>
                                </Pressable>
                            </View>
                            <Animated.View style={[styles.inputCont, {bottom: bottomSearchAnim}]}>
                                <TextInput onBlur={onScreenTouch} ref={inputEl} style={styles.inputSearch}/>
                            </Animated.View>
                        </View>
                    </Pressable>
            </Animated.View>
    )
}

export default SectionHeader;

export interface props {
    title: string,
    scrollY: Animated.Value,
}

const styles = StyleSheet.create({
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
    },
    headerCont: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        height: HEADER_MAX_HEIGHT,
    },
    headerElCont:{
        flex: 1,
        backgroundColor: '#131215',
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: '100%',
        zIndex: 1,
    },
    headerTitle: {
        right: TITLE_MAX_MARGIN,
        bottom: 15,
        fontSize: 30,
    },
    imageSearchCont: {
        position: 'absolute',
        bottom: 18,
        right: 15,
    },
    imageSearch:{
        width: MAX_SEARCH_SZ,
        height: MAX_SEARCH_SZ,
    },
    inputCont: {
        flex: 1,
        alignItems: 'center',
        position: 'absolute',
        width: '100%'
    },
    inputSearch: {
        backgroundColor: '#FFFFFF',
        width: '90%',
        borderRadius: 10,
        paddingLeft: 10,
    },
  });