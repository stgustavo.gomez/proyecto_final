import React from 'react';
import PropTypes from "prop-types";
import { Image, StyleSheet, Text, View } from 'react-native';


const TabIcon= ({name, focused}: params) => {

    let iconImage;

    if (name === 'anecdotas') {
        iconImage = require('../assets/TabIcons/anecdotas.png')
    } else if (name === 'chat') {
        iconImage = require('../assets/TabIcons/chat.png')
    } else if (name === 'entradas') {
        iconImage = require('../assets/TabIcons/entradas.png')
    } else if (name === 'peliculas') {
        iconImage = require('../assets/TabIcons/peliculas.png')
    } else if (name === 'perfil') {
        iconImage = require('../assets/TabIcons/perfil.png')
    }

    let op = 1;
    op = focused ? 1 : .5; 
    
    return (
        <Image
        style={[styles.tinyLogo , {opacity: op}]}
        source={iconImage}
      />
    )
}

export default TabIcon;



interface params {
    name: string,
    focused: boolean,
}


const styles = StyleSheet.create({
    
    tinyLogo: {
      width: 30,
      height: 28,
    },
    
  });