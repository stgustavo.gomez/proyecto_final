

import React from 'react'
import { View, StyleSheet, ListRenderItem } from 'react-native'
import { Tabs, MaterialTabBar, useCollapsibleStyle } from 'react-native-collapsible-tab-view'


import MovieHeader from '../components/MovieHeader';

import ResumeScreen from '../MainSections/PeliculaScreens/ResumeScreen'
import OpinionsScreen from '../MainSections/PeliculaScreens/OpinionsScreen'
import VotesScreen from '../MainSections/PeliculaScreens/VotesScreen'

//const HEADER_HEIGHT = 800

const PeliculaNav = ({title}: PeliculaNavProps) => {

    const currentHeader = () => {
        return <MovieHeader title={title}/>
    }

    
  
  return (
    <Tabs.Container
      revealHeaderOnScroll
      HeaderComponent={currentHeader}
      minHeaderHeight={100}
      containerStyle={{backgroundColor: '#131215'}}
      TabBarComponent={(props) => (
        <MaterialTabBar
          {...props}
          labelStyle={{ fontSize: 15 }}
          style={{backgroundColor: '#131215'}}
          indicatorStyle={{ backgroundColor: 'grey'}}
          
        />
      )}
    >
        <Tabs.Tab name="RESUMEN">
            <Tabs.ScrollView>
                <ResumeScreen/>
            </Tabs.ScrollView>
        </Tabs.Tab>
        <Tabs.Tab name="OPINIONES">
            <Tabs.ScrollView>
                <OpinionsScreen/>
            </Tabs.ScrollView>
        </Tabs.Tab>
        <Tabs.Tab name="VOTOS">
            <Tabs.ScrollView>
                <VotesScreen/>
            </Tabs.ScrollView>
        </Tabs.Tab>
    </Tabs.Container>
  )
}

const styles = StyleSheet.create({
  
})


interface PeliculaNavProps {
  title: string
}

export default PeliculaNav

