import 'react-native-gesture-handler';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

import Peliculas from '../MainSections/Peliculas'
import Pelicula from '../MainSections/Pelicula'

const Stack = createStackNavigator();

const PeliculasNav = () => {

    return (
        <Stack.Navigator  initialRouteName="Peliculas">
            <Stack.Screen name="Peliculas" options={{ title: 'Peliculas', headerShown: false }} component={Peliculas} />
            <Stack.Screen name="Pelicula" options={{ title: 'Pelicula', headerShown: false  }} component={Pelicula} />
        </Stack.Navigator>
    )
}

export default PeliculasNav;