import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';

import { Tabs, useCollapsibleStyle } from 'react-native-collapsible-tab-view'


const ResumeScreen = () => {

    const {
        contentContainerStyle,
        progressViewOffset,
        style,
      } = useCollapsibleStyle()


    return (
        <View  style={styles.container}>
           
            <View style={{flex: 1, flexDirection: 'row', paddingVertical: 20}}>

                <View style={{}}>
                    <Text style={styles.txtListLeft}>Año</Text>
                    <Text style={styles.txtListLeft}>Duración</Text>
                    <Text style={styles.txtListLeft}>País</Text>
                    <Text style={styles.txtListLeft}>Dirección</Text>
                    <Text style={styles.txtListLeft}>Premios</Text>
                </View>

                <View style={styles.ListRight}>
                    <Text style={styles.txtListRight}>2016</Text>
                    <Text style={styles.txtListRight}>111 min.</Text>
                    <Text style={styles.txtListRight}>Estados Unidos</Text>
                    <Text style={styles.txtListRight}>Barry Jenkins</Text>
                    <Text style={styles.txtListRight}>2 Globos de Oro</Text>
                </View>

            </View>

            <Text style={styles.description}>
                Chiron es un joven afroamericano con una difícil infancia y adolescencia, que crece en una zona conflictiva de Miami. La película trata la vida de Chiron, un chico afroamericano que crece en uno de los barrios más conflictivos de Estados Unidos. {"\n\n"}Desde su infancia, hasta su adolescencia y la etapa adulta, Chiron trata de encontrar su lugar en el mundo, en un ambiente de violencia. {"\n\n"}

                Chiron es un joven afroamericano con una difícil infancia y adolescencia, que crece en una zona conflictiva de Miami. La película trata la vida de Chiron, un chico afroamericano que crece en uno de los barrios más conflictivos de Estados Unidos. {"\n\n"}Desde su infancia, hasta su adolescencia y la etapa adulta, Chiron trata de encontrar su lugar en el mundo, en un ambiente de violencia.

                
            </Text>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 38,
        zIndex: 1,
        backgroundColor: '#131215',
    },
    txtListLeft: {
        fontSize: 15,
        paddingTop: 3,
        color: '#FFFFFF'
    },
    ListRight: {
        paddingLeft: 60,
    },
    txtListRight: {
        fontSize: 15,
        paddingTop: 3,
        color: 'rgba(255, 255, 255, 0.4)'
    },
    description: {
        fontSize: 15,
        lineHeight: 20,
        paddingBottom: 50,
        color: '#FFFFFF'
    }
})

export default ResumeScreen;