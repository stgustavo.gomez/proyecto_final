import React, {useRef} from 'react';
import { StyleSheet, Text, View, ScrollView, Animated } from 'react-native';

import PeliculaNav from '../Navs/PeliculaNav';

const HEADER_MAX_HEIGHT = 200;

const Pelicula = () => {

    const scrollY = useRef(new Animated.Value(0)).current;

    return (
        <View style={{ height: '100%' }}>
             

            <PeliculaNav title={'Moonlight'}/>
           
        </View>
    )
}

export default Pelicula;


const styles = StyleSheet.create({
    content: {
        marginTop: HEADER_MAX_HEIGHT,
       
    },
    
   
  });