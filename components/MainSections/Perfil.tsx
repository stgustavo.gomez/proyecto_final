import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';


const Perfil = ({logout}: PerfilProps) => {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Button title={"LogOut"} onPress={logout}/>
        </View>
    )
}

export interface PerfilProps {
    logout?: Function,
}


export default Perfil;