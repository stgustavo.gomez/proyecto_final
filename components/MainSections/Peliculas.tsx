import React, {useRef} from 'react';
import { ScrollView, StyleSheet, Text, View, Animated, Pressable, Button } from 'react-native';

import SectionHeader from '../components/SectionHeader';
import MovieList from '../components/MovieList'

const HEADER_MAX_HEIGHT = 150;

const Peliculas = () => {

    const scrollY = useRef(new Animated.Value(0)).current;

    return(
        <View style={styles.containter}>
             
            <ScrollView 
                scrollEventThrottle={16}
                onScroll={Animated.event(
                    [{nativeEvent: {contentOffset: {y: scrollY}}}],
                    {useNativeDriver: false}
                )}
                >
                    
                    <View style={styles.scroll}>

                        <MovieList titleList={'Premiadas'}/>
                        <MovieList titleList={'Thrillers'}/>
                        <MovieList titleList={'Más votadas Filmaffinity'}/>
                        <MovieList titleList={'Premiadas'}/>
                        <MovieList titleList={'Premiadas'}/>
                        <MovieList titleList={'Thrillers'}/>
                        <MovieList titleList={'Más votadas Filmaffinity'}/>
                        <MovieList titleList={'Premiadas'}/>

                    </View>
                
            </ScrollView>

            <SectionHeader scrollY={scrollY} title={'PELÍCULAS'}/>

            
        </View>
    )
}

const styles = StyleSheet.create({
    containter:{
        backgroundColor: '#131215'
    },
    scroll:{
        marginTop: HEADER_MAX_HEIGHT,
    }

});

export default Peliculas;