import 'react-native-gesture-handler';
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, TextInput, Button, Pressable } from 'react-native';

import Constants from 'expo-constants';



//IMPORTACION DE REACT NAVIGATOR
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';

//IMPORTACIÓN DE COMPONENTES
import PeliculasNav from './components/Navs/PeliculasNav';
import Entradas from './components/MainSections/Entradas';
import Anecdotas from './components/MainSections/Anecdotas';
import Perfil from './components/MainSections/Perfil';
import TabIcon from './components/TabIcon';

// FIREBASE
import * as firebase from 'firebase';

var firebaseConfig = {
    apiKey: "AIzaSyA3JZLCnbTjVi66QeMi0D_FZ1AYnP4MGfg",
    authDomain: "practica12-6df4a.firebaseapp.com",
    projectId: "practica12-6df4a",
    storageBucket: "practica12-6df4a.appspot.com",
    messagingSenderId: "465741271067",
    appId: "1:465741271067:web:bc4aae8199f23c0c5be8be",
    measurementId: "G-L2DZQK6ZPX"
};



function LoadingScreen(){
  return <View style={styles.container}>
            <ActivityIndicator />
          </View>
}

function FormScreen({onRegister, onLogin, error}){
    const [email, setEmail] = React.useState('')
    const [password, setPassword] = React.useState('')
    return <View style={styles.container}>
            <TextInput autoCapitalize={'none'} style={styles.InputTxt} placeholder="Email" onChangeText={setEmail} value={email} />
            <TextInput autoCapitalize={'none'} style={styles.InputTxt} placeholder="Password" onChangeText={setPassword} value={password} secureTextEntry={true} />
            <Pressable style={[styles.button, {marginTop: 40}]} onPress={() => onRegister(email, password)}>
               <Text style={styles.buttonTxt}>REGISTER</Text> 
            </Pressable>
            <Pressable style={styles.button} onPress={() => onLogin(email, password)}>
               <Text style={styles.buttonTxt}>LOGIN</Text> 
            </Pressable>
           
            {!!error && <Text style={styles.error}> Error: {error} </Text>}
        </View>
}


const Tab = createBottomTabNavigator();


const MainNav = ({logout}: MainNavProps) => {

  const PerfilScreen = () => {
    return (
      <Perfil logout={logout} />
    )
  }

  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <Tab.Navigator 
            
             screenOptions={({ route }) => ({
              tabBarIcon: ({ focused, color, size }) => {
                let iconName = '';
    
                if (route.name === 'Anecdotas') {
                  iconName = 'anecdotas';
                } else if (route.name === 'Chat') {
                  iconName = 'chat';
                } else if (route.name === 'Entradas') {
                  iconName = 'entradas';
                } else if (route.name === 'Peliculas') {
                  iconName = 'peliculas';
                } else if (route.name === 'Perfil') {
                  iconName = 'perfil';
                }

                // You can return any component that you like here!
                return <TabIcon name={iconName} focused={focused}/>;
                //return <Ionicons name={iconName} size={size} color={color} />;
              },
            })}
            tabBarOptions={{
              activeTintColor: ' rgba(255, 255, 255, .7)',
              inactiveTintColor: ' rgba(255, 255, 255, .5)',
              safeAreaInsets: {bottom: 10},
              style: {position: 'relative', backgroundColor: '#131215'}
            }}
           
            
            
            initialRouteName="Peliculas">
          <Tab.Screen name="Peliculas" options={{ title: 'Peliculas' }} component={PeliculasNav} />
          <Tab.Screen name="Anecdotas" options={{ title: 'Anécdotas' }} component={Anecdotas} />
          <Tab.Screen name="Chat" options={{ title: 'Chat' }} component={Entradas} />
          <Tab.Screen name="Entradas" options={{ title: 'Entradas' }} component={Entradas} />
          <Tab.Screen name="Perfil" options={{ title: 'Perfil' }} component={PerfilScreen} />
        </Tab.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  )
}





export default function App() {

  const [loading, setLoading] = React.useState(true)
  const [error, setError] = React.useState('')
  const [token, setToken] = React.useState('')
  const [user, setUser] = React.useState(null)
  React.useEffect(()=>{
    firebase.apps.length === 0 && firebase.initializeApp(firebaseConfig);
    firebase.auth().onAuthStateChanged(user => {
      if (user != null) {
        console.log('We are authenticated now!', user.providerData);
        setUser({uid: user.uid, displayName: user.displayName, email: user.email})
        setError('')
      }
      setLoading(false)
    });
  }, [])

  async function register(email, password){
    try {
      const user = await firebase.auth().createUserWithEmailAndPassword(email, password)
     
    } catch ({message}){
      setError(message)
    }
  }

  async function login(email, password){
    try {
      const user = await firebase.auth().signInWithEmailAndPassword(email, password)
     
    } catch ({message}){
      setError(message)
    }
  }

  async function logout() {
    firebase.auth().signOut().then(() => {
      setError('')
      setUser(null)
      setLoading(false)
    }).catch((error) => {
      setError(error.message)
    });
  }

  if (loading){
    return <LoadingScreen />
  }


  return ( 
          <>
            {user
              ?<MainNav logout = {logout}/>
              :<FormScreen error={error} onLogin={login} onRegister={register} />
            }
          </>
         );
}



export interface MainNavProps{
  logout: Function,
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#131215',
    padding: 8,
  },
  text: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 22
  },
  error: {
    color: 'red'
  },
  InputTxt: {
    width: '100%',
    paddingVertical: 5,
    paddingLeft: 10,
    marginTop: 10,
  },
  button: {
    alignSelf: 'center',
    marginTop: 10,
    width: '100%',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#FFFFFF',
    borderRadius: 5,
  },
  buttonTxt: {
    textAlign: 'center',
    paddingVertical: 10,
  }
});

